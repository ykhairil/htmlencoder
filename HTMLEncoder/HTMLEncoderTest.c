//
//  HTMLEncoderTest.c
//  HTMLEncoder
//
//  Created by syed on 15/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "HTMLEncoderTest.h"
#include "HTMLEncoder.h"
#include <string.h>
#include <stdlib.h>

void test_tag_with_text()
{
    const char* test_string="<font color=\"red\" size=\"1\">test satu</font>";
    unsigned long compress_size = 0;
    unsigned long error = 0;
    unsigned long output_size = 0;
    char* encoding = NULL;
    encoding = compress_html(test_string, strlen(test_string), &compress_size, &output_size, &error);
    printf("%s",encoding);
    free(encoding);
}

void run_all_test()
{
    test_tag_with_text();
}
