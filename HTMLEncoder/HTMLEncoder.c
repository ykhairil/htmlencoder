//
//  HTMLEncoder.c
//  HTMLEncoder
//
//  Created by yussairi on 29/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "HTMLEncoder.h"
#include "htmlsymbols.h"
#include <string.h>
#include <stdlib.h>

void analyze_value(int attrib_type, unsigned char* output_code_value)
{
    //output bits is flagged based on what their value type is
    //[00] character
    //[01] signed decimal
    //[10] unsigned decimal
    //[11] hexadecimal
    //
    //[00] single, treat the following 8 bit as a single data
    //[01] plural, treat the following 8 bit as array size n, then read n bytes
    //[10] delimited, treat the following 8 bit as array size n, the next 8 bit as delimiting char, then read n bytes
    //[11] -reserved-
    if (attrib_type == 0)
    {
        //ABBR is CHARACTER PLURAL, since it have no lookup table
    }
    else if (attrib_type == 1)
    {
        //ACCEPT is UNSIGNED DECIMAL DELIMITED, since it can be a series of lookup index delimited by a ','
    }
}

unsigned long add_text_code(const char* input_string, int input_length, unsigned long* current_code_count, unsigned char* output_code_values, int* output_code_types)
{
    //natural text structure : 8 bit per char as usual
    
    unsigned long result=0;
    
    for (int i=0; i < input_length; ++i)
    {
        output_code_types[*current_code_count]=nl_text;
        output_code_values[*current_code_count]=input_string[i];
        (*current_code_count)++;
    }
    return result;
}

unsigned long add_value_code(const char* input_string, int input_length, unsigned long* current_code_count, unsigned char* output_code_values, int* output_code_types)
{
    //attribute value code structure
    //[hhhhhhhh][]
    unsigned long result=0;
    
    //todo: analyze nature of value data
    //to determine how it can be compressed
    int data_type = attrib_data_string;
    
    if (data_type == attrib_data_string)
    {
        //treat data as natural text
        result = add_text_code(input_string, input_length, current_code_count, output_code_values, output_code_types);
    }
    else if (data_type == attrib_data_unsigned_number)
    {
        
    }
    
    return result;
}

unsigned long add_tag_code(const char* input_string, unsigned long* current_code_count, unsigned char* output_code_values, int* output_code_types)
{
    //html tag code structure : 16 bit wide
    //[h][ddddddd][nnnnnnnn]
    //h = bit flag signifying whether it's an open or closed tag
    //d = number for tag index
    //n = number of attributes set
    unsigned long result=0;
    
    
    //get index from tag code table
    for (int i=0; i < html_tags_size; ++i)
    {
        if (strcmp(input_string, html_tags[i])==0)
        {
            output_code_types[*current_code_count]=html_tag;
            output_code_values[*current_code_count]=i;
            (*current_code_count)++;
            result = i;
            break;
        }
    }
    return result;
}

unsigned long add_attribute_name_code(const char* input_string, unsigned long* current_code_count, unsigned char* output_code_values, int* output_code_types)
{
    //html attribute name code structure : 16 bit wide
    //[ddddddddd][nnnnnnn]
    //d = number for attrib index
    //n = length of attrib value
    unsigned long result=0;
    
    //get index from attrib name code table
    for (int i=0; i < html_attribs_size; ++i)
    {
        if (strcmp(input_string, html_attribs[i])==0)
        {
            output_code_types[*current_code_count]=html_attribute_name;
            output_code_values[*current_code_count]=i;
            (*current_code_count)++;
            result = i;
            break;
        }
    }
    return result;
}

void encode(unsigned char* output_code_values, int* output_code_types, int* output_code_element_counts, unsigned char* output_string)
{
    //analyze number of attribute and length of attribute value to
    //determine bit width of tag's attribute count and
    //attribute's value size
}

unsigned long recurse_parse_tag(const char* input_string, unsigned long input_size, int parse_state, int current_substring_start_pos, int current_substring_end_pos)
{
    //state movement example
    //    <img src="a.jpg" value="a">text</img>
    //NLT N                         N....N
    //TAG T...T                     T    T....T
    //ATR     A...A       A.....A
    //VAL         VV.....VV     VV.VV
    
    int current_substring_search_pos = current_substring_start_pos;
    
    while (current_substring_search_pos < current_substring_end_pos)
    {
        //process start symbols
        if (input_string[current_substring_search_pos] == '<')
        {
            return recurse_parse_tag(input_string + current_substring_search_pos, input_size, html_tag, current_substring_search_pos, current_substring_end_pos);
        }
        
        //process end symbols
        if (input_string[current_substring_search_pos] == '>')
        {
            return recurse_parse_tag(input_string + current_substring_search_pos, input_size, nl_text, current_substring_search_pos, current_substring_end_pos);
        }
        ++current_substring_search_pos;
    }
    
    return 0;
}

unsigned long parse_tags(const char* input_string, unsigned long input_size, int parse_state, unsigned char* output_code_values, int* output_code_types, int* output_code_element_counts)
{    
    //state movement example
    //    <img src="a.jpg" value="a">text</img>
    //NLT N                         N....N
    //TAG T...T                     T    T....T
    //ATR     A...A       A.....A
    //VAL         VV.....VV     VV.VV
    int current_substring_start_pos = 0;
    int current_substring_end_pos = 0;
    int current_substring_search_pos = 0;
    
    //Accumulate count of codable data found here
    unsigned long code_found_count = 0;
    unsigned long attribs_found=0;
    
    //define initial parse state
    int current_parse_state = parse_state;
    
    while (current_substring_search_pos < input_size)
    {
        //while parse state is in nl_text, everything is treated
        //as natural language content
        //start with start symbols first
        if (input_string[current_substring_search_pos] == '<')
        {
            if (current_parse_state == nl_text)
            {
                //get end of string
                current_substring_end_pos=current_substring_search_pos;
                
                //tokenize string and encode based on current state
                int substring_length = current_substring_end_pos - current_substring_start_pos;
                if (substring_length > 0)
                {
                    char* token = NULL;
                    token = calloc(substring_length, sizeof(char));
                    memcpy(token, input_string + current_substring_start_pos, substring_length);
                    printf("Text: %s\n",token);
                    add_text_code(token, substring_length, &code_found_count, output_code_values, output_code_types);
                    free(token);
                }
                //shift to html tag state
                current_parse_state = html_tag;
                current_substring_start_pos=current_substring_search_pos+1;
            }
            
        }
        else if (input_string[current_substring_search_pos] == '"')
        {
            //toggle value input state
            if (current_parse_state == html_tag)
            {
                current_parse_state = html_attribute_value;
                current_substring_start_pos = current_substring_search_pos+1;
            }
            else if (current_parse_state == html_attribute_value)
            {
                //get end of string
                current_substring_end_pos=current_substring_search_pos;
                
                //tokenize string and encode based on current state
                int substring_length = current_substring_end_pos - current_substring_start_pos;
                if (substring_length > 0)
                {
                    char* token = NULL;
                    token = calloc(substring_length, sizeof(char));
                    memcpy(token, input_string + current_substring_start_pos, substring_length);
                    printf("\tValue: %s\n",token);
                    add_value_code(token, substring_length, &code_found_count, output_code_values, output_code_types);
                    free(token);
                }
                current_substring_start_pos = current_substring_search_pos+1;
                current_parse_state = html_tag;
            }
            
        }
        
        //process escape chars
        if (input_string[current_substring_search_pos] == '\\')
        {
            current_substring_start_pos++;
        }
        
        //then process terminal symbol
        if (input_string[current_substring_search_pos] == '>')
        {
            
            //get end of string
            current_substring_end_pos=current_substring_search_pos;
            
            //tokenize string and encode based on current state
            int substring_length = current_substring_end_pos - current_substring_start_pos;
            if (substring_length > 0)
            {
                char* token = NULL;
                token = calloc(substring_length, sizeof(char));
                memcpy(token, input_string + current_substring_start_pos, substring_length);
                
                //test for tag ending
                int tag_end_shift = 0;
                if (token[0]=='/')
                    tag_end_shift=1;
                
                if (attribs_found==0)
                {
                    add_tag_code(token + tag_end_shift, &code_found_count, output_code_values, output_code_types);
                    printf("Tag: %s\n",token);
                }
                else
                {
                    add_attribute_name_code(token, &code_found_count, output_code_values, output_code_types);
                    printf("Attribute: %s\n",token);
                }
                free(token);
            }
            //shift to natural text state
            current_parse_state = nl_text;
            current_substring_start_pos=current_substring_search_pos+1;
            current_substring_end_pos=current_substring_search_pos+1;
            
            //reset attribute count
            attribs_found = 0;
        }
        else if (input_string[current_substring_search_pos] == ' ')
        {
            //shift to html attribute state if currently within html tag state
            if (current_parse_state == html_tag)
            {
                //get end of string
                current_substring_end_pos=current_substring_search_pos;
                
                //tokenize string and encode based on current state
                int substring_length = current_substring_end_pos - current_substring_start_pos;
                if (substring_length > 0)
                {
                    char* token = NULL;
                    token = calloc(substring_length, sizeof(char));
                    memcpy(token, input_string + current_substring_start_pos, substring_length);
                    if (attribs_found==0)
                    {
                        add_tag_code(token, &code_found_count, output_code_values, output_code_types);
                        printf("Tag: %s\n",token);
                    }
                    else
                    {
                        add_attribute_name_code(token, &code_found_count, output_code_values, output_code_types);
                        printf("Attribute: %s\n",token);
                    }
                    free(token);
                    current_substring_start_pos = current_substring_search_pos+1;
                    ++attribs_found;
                }
                else
                {
                    current_substring_start_pos = current_substring_search_pos+1;
                }
            }
        }
        else if (input_string[current_substring_search_pos] == '=')
        {
            //shift to html tag state
            if (current_parse_state == html_tag)
            {
                //get end of string
                current_substring_end_pos=current_substring_search_pos;
                
                int substring_length = current_substring_end_pos - current_substring_start_pos;
                if (substring_length > 0)
                {
                    char* token = NULL;
                    token = calloc(substring_length, sizeof(char));
                    memcpy(token, input_string + current_substring_start_pos, substring_length);
                    if (attribs_found==0)
                    {
                        add_tag_code(token, &code_found_count, output_code_values, output_code_types);
                        printf("Tag: %s\n",token);
                    }
                    else
                    {
                        add_attribute_name_code(token, &code_found_count, output_code_values, output_code_types);
                        printf("Attribute: %s\n",token);
                    }
                    free(token);
                    current_substring_start_pos = current_substring_search_pos+1;
                    ++attribs_found;
                }
            }
        }
        
        ++current_substring_search_pos;
    }
    
    return code_found_count;
}

char* compress_html(const char* input_string, unsigned long input_size, unsigned long* compress_size, unsigned long* output_size, unsigned long* error)
{
    //results array
    //allocate based on assumption that maximum possible result is equal to sum of chars in string
    unsigned char* code_values = NULL;
    int* code_types = NULL;
    int* code_element_counts = NULL;
    unsigned long code_found_count = 0;
    code_values = calloc(input_size, sizeof(int));
    code_types = calloc(input_size, sizeof(int));
    code_element_counts = calloc(input_size, sizeof(int));
    
    //code_found_count = parse_tags(input_string, input_size, &code_values[0], &code_types[0]);
    code_found_count = parse_tags(input_string, input_size, nl_text, &code_values[0], &code_types[0], &code_element_counts[0]);
    
    //allocate output twice the size of input for safety
    //actual count is returned in output_size
    char* output_code = calloc(code_found_count*2, sizeof(char));
    //unsigned long output_size=0;
    //output_size = encode_variable_width(&code_values[0], &code_types[0], &code_capstyle[0], code_found_count, &output_code[0]);
    
    free(code_values);
    free(code_types);
    
    if (output_size==0)
    {
        free(output_code);
        return NULL;
    }
    else
    {
        //*compress_size = output_size;
        *output_size = code_found_count;
        return output_code;
    }
}

