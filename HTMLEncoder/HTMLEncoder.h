//
//  HTMLEncoder.h
//  HTMLEncoder
//
//  Created by yussairi on 29/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef HTMLEncoder_h
#define HTMLEncoder_h

#include <stdio.h>

char* compress_html(const char* input_string, unsigned long input_size, unsigned long* compress_size, unsigned long* output_size, unsigned long* error);

#endif /* HTMLEncoder_h */
