# HTMLEncoder

A C library for compressing HTML text using frequency-based block coding. This encoding algorithm is best suited for compressing static web content that would not be compressed efficiently with other frequency-based encoding algorithm such as Lempel-Ziv or DEFLATE (used in zlib and zip). However, this worked only on HTML grammars; content that is identified as natural language text is left as is and can be compressed in another way, such as by using another library in the repository, NLTextEncoder.

## Usage

To use this library in your project, simply add the header codes and the source codes in HTMLEncoder folder to your project. 

The algorithm can be compiled using gcc, msvc, clang or any other C99-compatible C compilers.


## Theory
The algorithm worked by finding repetitive patterns in a HTML text file and representing these repetitions as a reduced data. The most obvious repetitions in a HTML text file is the use of HTML tags and attributes. Therefore, by coding the entire symbol of HTML into a code table and assigning short numbers to it, the algorithm is able to highly reduce the size of data in the HTML file.
